from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from cms.models import Campaign
from django.contrib.sites.models import Site


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


def user_login(request):
    errors = []
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                # Redirect to a success page.
                #return render(request, 'dash.html')
                return HttpResponseRedirect("/dash")
            else:
                # Return a 'disabled account' error message
                errors.append('disabled account')
        else:
            # Return an 'invalid login' error message..
            errors.append('Invalid login')

    return render(request, 'login.html', {'errors': errors})


def logout_view(request):
    logout(request)
    # Redirect to a success page.
    #return render(request, 'login.html')
    return HttpResponseRedirect("/login")

@login_required(login_url="/login")
def dash_view(request):
    if request.method == 'POST':
        if request.POST.get('logout', ''):
            #call logout view
            return logout_view(request)
    campaigns = Campaign.objects.all()
    return render(request, 'dash.html', {'campaigns': campaigns})



