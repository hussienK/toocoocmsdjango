from django.test import TestCase
from django.test import Client




__author__ = 'root'


class TestUser_Login(TestCase):
    fixtures = ['initial_data.json']

    """
    Test the functionality of the login system
    by testing the following:
    1) Correct username/password
    2) incorrect username/password
    3) Correct username/empty password
    4) Empty username and correct password
    5) Verify error message
    6) Verify that logging out and pushing back won't take you back to your logged in page.
    7) Test a page that needs you to be logged in without logging in.
    8) verify session timeout
    9) Verify SQL injection
    """

    def setUp(self):
        pass
        #setup an entry for a new user.
        #self.user = User.objects.create_user(username='test', password='test')


    def test_user_login(self):
        #create an instance of the client object
        client = Client()


        #1) Correct username/password
        #create dummy web request to the login page, test the following:
        response = client.post('/login/', {'username': 'admin', 'password': 'admin'})
        self.assertEqual(response.status_code, 302)

        client1 = Client()
        #2) Incorrect username/password
        response = client1.post('/login/', {'username': 'test3', 'password': 'test2'})
        self.assertEqual(response.status_code, 200)

        #3) Correct username/empty password
        client2 = Client()
        response = client2.post('/login/', {'username': 'admin', 'password': ''})
        self.assertEqual(response.status_code, 200)

        #4) Empty username and correct password
        client3 = Client()
        response = client3.post('/login/', {'username': '', 'password': 'admin'})
        self.assertEqual(response.status_code, 200)

        #5) Verify error message
        #testing for error message: "invalid login"
        #test client4 with invalid login credentials then check for error msg
        client4 = Client()
        response = client4.post('/login/', {'username': 'admin', 'password': 'admin3'})
        self.assertEqual(response.status_code, 200)
        self.assertInHTML("<li>Invalid login</li>", response.content)


        #6) Verify that logging out and pushing back won't take you back to your logged in page.
        #7) Test a page that needs you to be logged in without logging in.
        #8) verify session timeout
        #9) Verify SQL injection
