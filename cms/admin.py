from django.contrib import admin
from cms.models import Campaign


class CampaignAdmin(admin.ModelAdmin):
    list_display = ('id', 'campaign', 'header', 'title')


admin.site.register(Campaign)
