from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Campaign(models.Model):
    id = models.IntegerField(primary_key=True)
    campaign = models.CharField(max_length=255)
    header = models.TextField()
    title = models.TextField()
    simage = models.TextField()
    body = models.TextField()
    anchor1 = models.TextField()
    link1 = models.TextField()
    anchor2 = models.TextField()
    link2 = models.TextField()
    offerimg1 = models.TextField()
    offerimg2 = models.TextField()
    gacode = models.TextField()
    exit1 = models.TextField()
    exit2 = models.TextField()
    templateid = models.IntegerField(db_column='templateID')  # Field name made lowercase.
    fimage = models.TextField()
    simage2 = models.TextField()
    mimage = models.TextField()
    cimage = models.TextField()
    revision = models.TextField()



